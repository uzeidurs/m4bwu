## May the 4th [be with you] ##

A custom voting component made with Fuse.

![M4BWU](./M4BWU.png)

All code in this repo is public domain - which means you can use it for whatever you want.
https://creativecommons.org/publicdomain/zero/1.0/
